package main

import (
	"fmt"
	"github.com/gorilla/mux"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"gitlab.com/golang110/go-bookstore/pkg/routes"
	"log"
	"net/http"
)

func main(){
	r := mux.NewRouter()
	routes.RegisterBookStoreRoutes(r)
	http.Handle("/",r)
	fmt.Println("hi")
	log.Fatal( http.ListenAndServe("localhost:9000",r) )
}
